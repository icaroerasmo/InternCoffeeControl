package coffee.intern.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public abstract class Employee {
	
	protected Integer id;
	
	@NotNull
	@Size(min=3, max=8)
	protected String name;
	
	public Employee() {}
	
	public Employee(String name){
		this.name = name;
	}
	
	public abstract Integer getId();
	public abstract void setId(Integer id);
	public abstract String getName();
	public abstract void setName(String name);

}
