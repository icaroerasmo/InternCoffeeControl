package coffee.intern.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Coffee {
	
	private Integer id;
	private Intern intern;
	private Date date;

	public Coffee(){
	}
	
	public Coffee(Intern intern) {
		this.date = new Date();
		this.intern = intern;
	}
	
	@Id
	@Column(name = "XPK_COFFEE", nullable = false)
	@SequenceGenerator(name = "SQ_COFFEE", sequenceName = "SQ_COFFEE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_COFFEE")
	public Integer getId() {
		return id;
	}

	@ManyToOne(targetEntity = Intern.class)
	@JoinColumn(name="XFK_INTERN", nullable=false)
	public Intern getIntern() {
		return intern;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	public Date getDate() {
		return date;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public void setIntern(Intern intern) {
		this.intern = intern;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
