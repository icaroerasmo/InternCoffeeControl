package coffee.intern.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import coffee.intern.enums.RoleEnum;
import coffee.intern.enums.UserStatusEnum;

@Entity
@Table(name="EMPLOYEE_USER")
public class User extends Employee {

	@NotNull
	@Size(min=3, max=10)
	private String username;
	
	@NotNull
	@Size(min=6)
	private String password;
	
	private UserStatusEnum status;
	private Set<Role> roles;
	
	public User() {
		
	}
	
	public User(String name) {
		super(name);
	}
	
	@Id
	@Column(name = "XPK_USER", nullable = false)
	@SequenceGenerator(name = "SQ_USER", sequenceName = "SQ_USER", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_USER")
	public Integer getId() {
		return id;
	}
	
	@Column(nullable=false, unique=true)
	public String getUsername() {
		return username;
	}
	
	@Column(nullable=false)
	public String getPassword() {
		return password;
	}

	@Override
	@Column(name="NAME", nullable=false)
	public String getName() {
		return name;
	}
	
	@Column(name="STATUS", nullable=false)
	public Integer getStatus() {
		return status != null ? status.get() : null;
	}
	
	@ManyToMany
	@JoinTable(name = "User_Role", joinColumns = @JoinColumn(name = "XFK_USER"), inverseJoinColumns = @JoinColumn(name = "XFK_ROLE"))
	public Set<Role> getRoles(){
		return roles;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	public void setStatus(Integer status) {
		this.status = UserStatusEnum.getStatus(status);
	}
	
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	@Transient
	@JsonIgnore
	public boolean isAdmin() {
		if(roles != null) {
			return roles.stream().anyMatch(item -> item.getRole() == RoleEnum.ADMIN);
		}
		return false;
	}
}
