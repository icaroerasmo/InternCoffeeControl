package coffee.intern.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import coffee.intern.enums.SelectionStatusEnum;

@Entity
public class Selection {

	private Integer id;
	private Intern intern;
	private SelectionStatusEnum status;
	private Date date;
	
	public Selection() {
	}
	
	public Selection(Intern intern) {
		this.date = new Date();
		this.intern = intern;
	}
	
	@Id
	@Column(name = "XPK_SELECTION", nullable = false)
	@SequenceGenerator(name = "SQ_SELECTION", sequenceName = "SQ_SELECTION", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_SELECTION")
	public Integer getId() {
		return id;
	}
	
	@ManyToOne(targetEntity = Intern.class)
	@JoinColumn(name="XFK_INTERN", nullable=false)
	public Intern getIntern() {
		return intern;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(nullable=false)
	public SelectionStatusEnum getStatus() {
		return status;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	public Date getDate() {
		return date;
	}
	
	public void setIntern(Intern intern) {
		this.intern = intern;
	}

	public void setStatus(SelectionStatusEnum status) {
		this.status = status;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public boolean equals(Object obj) {
		
		Selection selection = (Selection)obj;
		
		if(selection.getIntern() != null && intern != null) {
			return intern.getId() == selection.getIntern().getId();
		}
		
		return false;
	}
}
