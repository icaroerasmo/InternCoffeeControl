package coffee.intern.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Intern extends Employee {
	
	private List<Coffee> coffees;
	private List<Selection> selections;
	private Integer weight;
	
	public Intern() {
		
	}
	
	public Intern(String name){
		super(name);
		this.weight = 0;
	}
	
	@Id
	@Column(name = "XPK_INTERN", nullable = false)
	@SequenceGenerator(name = "SQ_INTERN", sequenceName = "SQ_INTERN", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_INTERN")
	public Integer getId() {
		return id;
	}
	
	@JsonIgnore
	@OneToMany(mappedBy="intern", targetEntity = Coffee.class, cascade = CascadeType.ALL)
	public List<Coffee> getCoffees() {
		return coffees;
	}
	
	@JsonIgnore
	@OneToMany(mappedBy="intern", targetEntity = Selection.class, cascade = CascadeType.ALL)
	public List<Selection> getSelections() {
		return selections;
	}

	@Override
	@Column(nullable=false, unique=true)
	public String getName() {
		return name;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public void setCoffees(List<Coffee> coffees) {
		this.coffees = coffees;
	}
	
	public void setSelections(List<Selection> selections) {
		this.selections = selections;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		boolean equals = true;
		
		equals = equals && (((Intern)obj).getId().equals(this.getId()));
		equals = equals && (((Intern)obj).getName().equals(this.getName()));
		
		return equals && (obj instanceof Intern);
	}
}
