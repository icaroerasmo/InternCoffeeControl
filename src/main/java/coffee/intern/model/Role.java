package coffee.intern.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import coffee.intern.enums.RoleEnum;

@Entity
@Table(name = "ROLE")
public class Role {
	
	private Integer id;
	private RoleEnum role;
	
	public Role(){
		
	}
	
	public Role(RoleEnum role){
		this.role = role;
	}
	
	@Id
	@Column(name = "XPK_ROLE", nullable = false)
	@SequenceGenerator(name = "SQ_ROLE", sequenceName = "SQ_ROLE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_ROLE")
	public Integer getId() {
		return id;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(unique=true)
	public RoleEnum getRole() {
		return role;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public void setRole(RoleEnum role) {
		this.role = role;
	}	
}