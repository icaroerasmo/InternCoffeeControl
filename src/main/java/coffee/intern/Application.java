package coffee.intern;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import coffee.intern.enums.RoleEnum;
import coffee.intern.model.Coffee;
import coffee.intern.model.Intern;
import coffee.intern.model.Role;
import coffee.intern.model.User;
import coffee.intern.repository.RoleRepository;
import coffee.intern.repository.UserRepository;
import coffee.intern.service.CoffeeService;
import coffee.intern.service.InternService;
import coffee.intern.service.RoleService;
import coffee.intern.service.UserService;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }
    
    @Bean
    CommandLineRunner init(final RoleService roleService,
    		final UserService userService, final BCryptPasswordEncoder bCryptPasswordEncoder, final InternService internService, final CoffeeService coffeeService) {
    	return new CommandLineRunner() {

			@Override
			public void run(String... arg0) throws Exception {
				try {
					for(RoleEnum roleEnum : RoleEnum.values()) {
						roleService.save(new Role(roleEnum));
					}
					User admin = new User("Admin");
					admin.setUsername("admin");
					admin.setPassword("12345678");
					Role adminRole = roleService.findByRole(RoleEnum.ADMIN);
					Set<Role> roles = new HashSet<>();
					roles.add(adminRole);
					admin.setRoles(roles);
					admin.setStatus(1);
					userService.save(admin);
					
					User user = new User("User");
					user.setUsername("user");
					user.setPassword("12345678");
					Role userRole = roleService.findByRole(RoleEnum.USER);
					Set<Role> migRoles = new HashSet<>();
					migRoles.add(userRole);
					user.setRoles(migRoles);
					user.setStatus(1);
					userService.save(user);
					
					Intern barreiro = new Intern("barreiro");
					barreiro.setWeight(1);
					internService.save(barreiro);
	
					internService.save(new Intern("icaro"));
					internService.save(new Intern("erasmo"));
	
					Intern souza = new Intern("souza");
					List<Coffee> souzasCoffees = new ArrayList<Coffee>();
					souza.setWeight(1);
					Coffee souzasCoffee = new Coffee(souza);
					souzasCoffee.setDate(new Date());
					souzasCoffees.add(souzasCoffee);
					souza.setCoffees(souzasCoffees);
					internService.save(souza);
	
					Calendar calendar = Calendar.getInstance();
	
					internService.findAll().forEach(entity -> {
						calendar.add(Calendar.DAY_OF_MONTH, -1);
						Coffee coffee = new Coffee(entity);
						coffee.setDate(calendar.getTime());
						coffeeService.save(coffee);
					});
					
				}catch(Exception e) {
		    		
		    	}	
			}
    	};
    }
    
	@Bean
	CommandLineRunner migrate(final InternService internService, final CoffeeService coffeeService,
			final RoleRepository roleService, final UserRepository userService) {
			return new CommandLineRunner() {
	
				@Override
				public void run(String... args) throws Exception {
					
					try {
						
						User user = new User("User");
						user.setUsername("user");
						user.setPassword("12345678");
						Role userRole = roleService.findByRole(RoleEnum.USER);
						Set<Role> migRoles = new HashSet<>();
						migRoles.add(userRole);
						user.setRoles(migRoles);
						userService.save(user);
						
						Intern barreiro = new Intern("barreiro");
						barreiro.setWeight(1);
						internService.save(barreiro);
		
						internService.save(new Intern("icaro"));
						internService.save(new Intern("erasmo"));
		
						Intern souza = new Intern("souza");
						List<Coffee> souzasCoffees = new ArrayList<Coffee>();
						souza.setWeight(1);
						Coffee souzasCoffee = new Coffee(souza);
						souzasCoffee.setDate(new Date());
						souzasCoffees.add(souzasCoffee);
						souza.setCoffees(souzasCoffees);
						internService.save(souza);
		
						Calendar calendar = Calendar.getInstance();
		
						internService.findAll().forEach(entity -> {
							calendar.add(Calendar.DAY_OF_MONTH, -1);
							Coffee coffee = new Coffee(entity);
							coffee.setDate(calendar.getTime());
							coffeeService.save(coffee);
						});
					}catch(Exception e) {
						
					}
				}
			};
		}
    }