package coffee.intern.exceptions;

import javax.persistence.RollbackException;
import javax.validation.ConstraintViolationException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ConstraintViolationExceptionHandler extends ResponseEntityExceptionHandler {
 
    @ExceptionHandler(value= {RollbackException.class, ConstraintViolationException.class})
    @RequestMapping(produces = "application/json")
    protected ResponseEntity<Object> handleConflict(RollbackException ex, WebRequest request) {


    	if(ex.getCause() instanceof ConstraintViolationException) {
	    	
	    	ConstraintViolationException consVioExcp = ((ConstraintViolationException) ex.getCause());
	    	JSONArray array = new JSONArray();
	    	
	    	consVioExcp.getConstraintViolations().forEach(item -> {
				try {
					array.put(new JSONObject()
							.put("attribute", item.getPropertyPath().toString())
					        .put("message", item.getMessage()));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			});
			
	        return handleExceptionInternal(consVioExcp, array.toString(), 
	          new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    	}
    	return handleExceptionInternal(ex, ex.getMessage(), 
  	          new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}