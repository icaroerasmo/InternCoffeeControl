package coffee.intern.repository;

import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import coffee.intern.model.Intern;

public interface InternRepository extends CrudRepository<Intern, Integer>{
	
	@Query("SELECT intr " + 
			"FROM Intern intr " + 
			"LEFT JOIN intr.coffees coff " + 
			"GROUP BY intr " + 
			"ORDER BY intr.weight " + 
			"DESC, max(coff.date) " + 
			"ASC, intr DESC")
	List<Intern> getInternOrder(Pageable pageable);

	Intern findByName(String name);

}
