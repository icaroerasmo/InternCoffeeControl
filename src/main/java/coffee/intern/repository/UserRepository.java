package coffee.intern.repository;

import org.springframework.data.repository.CrudRepository;
import coffee.intern.model.User;

public interface UserRepository extends CrudRepository<User, Integer>{
	User findByUsername(String username);
}
