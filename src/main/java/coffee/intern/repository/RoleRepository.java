package coffee.intern.repository;

import org.springframework.data.repository.CrudRepository;

import coffee.intern.enums.RoleEnum;
import coffee.intern.model.Role;

public interface RoleRepository extends CrudRepository<Role, Integer>{
	Role findByRole(RoleEnum role);
}
