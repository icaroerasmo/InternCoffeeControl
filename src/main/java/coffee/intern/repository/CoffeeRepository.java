package coffee.intern.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import coffee.intern.model.Coffee;

public interface CoffeeRepository extends CrudRepository<Coffee, Integer> {
	@Query("SELECT c FROM Coffee c where c.date >= :thisDayLastWeek ORDER BY c.date ASC") 
    List<Coffee> findLastWeekCoffees(@Param("thisDayLastWeek") Date thisDayLastWeek);
	
	@Query("SELECT COUNT(coff) FROM Coffee coff INNER JOIN coff.intern intr WHERE intr.id = :internId")
	Integer counterOfCoffeesByIntern(@Param("internId") Integer internId);
	
	@Transactional
	@Modifying
	@Query("DELETE FROM Coffee coff WHERE coff.intern.id = :internId")
	void deleteCoffeeById(@Param("internId") Integer internId);
}
