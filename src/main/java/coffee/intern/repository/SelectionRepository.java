package coffee.intern.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import coffee.intern.model.Selection;

public interface SelectionRepository extends CrudRepository<Selection, Integer>{
	@Transactional
	@Modifying
	@Query("DELETE FROM Selection selec WHERE selec.intern.id = :internId")
	void deleteSelectionById(@Param("internId") Integer internId);
}
