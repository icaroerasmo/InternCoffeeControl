package coffee.intern.controller;

import java.util.List;
import org.springframework.http.ResponseEntity;

public interface ICrudController<T> {
	public ResponseEntity<List<T>> get();
	public ResponseEntity<T> get(Integer id);
	public ResponseEntity<T> save(T t);
	public ResponseEntity<T> edit(T t);
	public ResponseEntity<T> delete(Integer id);
}
