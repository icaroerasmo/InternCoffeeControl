package coffee.intern.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import coffee.intern.model.Coffee;
import coffee.intern.model.Intern;
import coffee.intern.service.CoffeeService;
import coffee.intern.service.InternService;

@RestController
@RequestMapping("/coffee")
public class CoffeeController{

	@Autowired
	private CoffeeService coffeeService;
	
	@Autowired
	private InternService internService;
	
	@RequestMapping("intern/{id}")
	public ResponseEntity<List<Coffee>> get(@PathVariable Integer id){

		if(id != null) {
			Intern intern = internService.findOne(id);
			if(intern != null) {
				return new ResponseEntity<List<Coffee>>(intern.getCoffees(), HttpStatus.OK);
			}
			return new ResponseEntity<List<Coffee>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Coffee>>(HttpStatus.BAD_REQUEST);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<Coffee>> get(){
		List<Coffee> coffees = coffeeService.findAll();
		if(coffees != null && coffees.size() > 0) {
			return new ResponseEntity<List<Coffee>>(coffees, HttpStatus.OK);
		}
		return new ResponseEntity<List<Coffee>>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="lastWeek")
	public ResponseEntity<List<Coffee>> getLastWeekCoffees(){
		List<Coffee> coffees = coffeeService.findLastWeekCoffees();
		if(coffees != null && coffees.size() > 0) {
			return new ResponseEntity<List<Coffee>>(coffees, HttpStatus.OK);
		}
		return new ResponseEntity<List<Coffee>>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="counterByIntern")
	public ResponseEntity<Integer> getCoffeeCounterByIntern(@RequestParam(name="internId") Integer internId){
		if(internId != null) {
			Integer counter = coffeeService.counterOfCoffeesByIntern(internId);
			if(counter != null) {
				return new ResponseEntity<Integer>(counter, HttpStatus.OK);
			}
			return new ResponseEntity<Integer>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
	}
	
}
