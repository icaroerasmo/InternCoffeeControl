package coffee.intern.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import coffee.intern.model.Intern;
import coffee.intern.service.InternService;
import coffee.intern.util.AuthUtil;

@RestController
@RequestMapping("/intern")
public class InternController extends AuthUtil implements ICrudController<Intern> {

	@Autowired
	private InternService internService;

	public InternController() {

	}

	@Override
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Intern>> get() {
		List<Intern> interns = internService.findAll();
		if (interns != null && interns.size() > 0) {
			return new ResponseEntity<List<Intern>>(interns, HttpStatus.OK);
		}
		return new ResponseEntity<List<Intern>>(HttpStatus.NOT_FOUND);
	}

	@Override
	@RequestMapping(method = RequestMethod.GET, value = "{id}")
	public ResponseEntity<Intern> get(@PathVariable Integer id) {
		if (id != null) {
			Intern intern = internService.findOne(id);
			if (intern != null) {
				return new ResponseEntity<Intern>(intern, HttpStatus.OK);
			}
			return new ResponseEntity<Intern>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Intern>(HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Intern> save(@RequestBody Intern intern) {

		if (intern != null) {
			if (isAdmin()) {
				try {
					return new ResponseEntity<Intern>(internService.save(intern), HttpStatus.OK);
				} catch (DataIntegrityViolationException e) {
					return new ResponseEntity<Intern>(HttpStatus.CONFLICT);
				}
			}
			return new ResponseEntity<Intern>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Intern>(HttpStatus.BAD_REQUEST);

	}

	@RequestMapping(method = RequestMethod.PATCH)
	public ResponseEntity<Intern> edit(@RequestBody Intern intern) {
		if (intern != null) {
			if (isAdmin()) {

				Intern internToSave = internService.findOne(intern.getId());

				if (intern.getId() != null && internToSave != null) {
					internToSave.setName(intern.getName());
					return new ResponseEntity<Intern>(internService.save(internToSave), HttpStatus.OK);
				}
			}
			return new ResponseEntity<Intern>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Intern>(HttpStatus.BAD_REQUEST);
	}

	@Override
	@RequestMapping(method = RequestMethod.DELETE, value = "{id}")
	public ResponseEntity<Intern> delete(@PathVariable Integer id) {
		if (isAdmin()) {
			if (id != null) {
				Intern intern = internService.findOne(id);
				if (intern != null) {
					internService.delete(intern);
					return new ResponseEntity<Intern>(HttpStatus.OK);
				}
			}
			return new ResponseEntity<Intern>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Intern>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<Intern> deleteMany(@RequestBody List<Intern> users) {
		if (isAdmin()) {
			if (users.size() > 0) {
				internService.delete(users);
				return new ResponseEntity<Intern>(HttpStatus.OK);
			}
			return new ResponseEntity<Intern>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Intern>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(method = RequestMethod.GET, path = "byName")
	public ResponseEntity<Intern> getByName(@RequestParam(name = "name") String name) {
		if (name != null) {
			Intern intern = internService.findByName(name);
			if (intern != null) {
				return new ResponseEntity<Intern>(intern, HttpStatus.OK);
			}
			return new ResponseEntity<Intern>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Intern>(HttpStatus.BAD_REQUEST);
	}
}
