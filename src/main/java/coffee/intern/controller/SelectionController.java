package coffee.intern.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import coffee.intern.enums.SelectionStatusEnum;
import coffee.intern.model.Coffee;
import coffee.intern.model.Intern;
import coffee.intern.model.Selection;
import coffee.intern.service.CoffeeService;
import coffee.intern.service.InternService;
import coffee.intern.service.SelectionService;

@RestController
@RequestMapping("/selection")
public class SelectionController {
	
	@Autowired
	private InternService internService;
	
	@Autowired
	private CoffeeService coffeeService;
	
	@Autowired
	private SelectionService selectionService;
	
	private static int index = 0;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<Selection>> get(){
		
		List<Intern> internList = internService.getCoffeeOrder(null);
		
		if(internList == null || internList.size() < 1 ) {
			return new ResponseEntity<List<Selection>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Selection>>(internList.stream().map(item -> new Selection(item)).collect(Collectors.toList()), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="registered")
	public ResponseEntity<List<Selection>> getRegisteredSelections(){
		
		List<Selection> selectionList = selectionService.findAll();
		
		if(selectionList == null || selectionList.size() < 1 ) {
			return new ResponseEntity<List<Selection>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Selection>>(selectionList, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="next")
	public ResponseEntity<Selection> getNext(){
		
		if(index >= internService.getCoffeeOrder(null).size()) {
			index = 0;
		}
		
		Pageable pageable = new PageRequest(index, 1);
		List<Intern> internList = internService.getCoffeeOrder(pageable);
		
		if(internList != null && internList.size() > 0) {
			return new ResponseEntity<Selection>(new Selection(internList.get(0)), HttpStatus.OK);
		}
		return new ResponseEntity<Selection>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Selection> post(@RequestBody Selection selection){
		
		if(selection != null && selection.getStatus() != null) {
			
    		List<Intern> internList = internService.getCoffeeOrder(new PageRequest(index, 1));
    		
    		if(internList != null && internList.size() > 0 &&
    				internList.get(0).equals(selection.getIntern())) {
    			
    			Intern intern = internList.get(0);
    			
	    		selection.getStatus().setPontuation(intern);
				
	    		if(selection.getStatus() == SelectionStatusEnum.COFFEE_MADE) {
		    		coffeeService.save(new Coffee(intern));
		    		index = 0;
	    		}else {
	    			++index;
	    		}
	    		
	    		intern = internService.save(intern);
    			selection.setIntern(intern);
	    		selectionService.save(selection);
				
				return new ResponseEntity<Selection>(selection, HttpStatus.OK);
    		}
    		return new ResponseEntity<Selection>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Selection>(HttpStatus.BAD_REQUEST);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="reset")
	public ResponseEntity<Void> reset() {
		index = 0;
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
