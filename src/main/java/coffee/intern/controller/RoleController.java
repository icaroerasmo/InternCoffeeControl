package coffee.intern.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import coffee.intern.model.Role;
import coffee.intern.service.RoleService;

@RestController
@RequestMapping("/role")
public class RoleController{

	@Autowired
	private RoleService roleService;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<Role>> get(){
		List<Role> roles = roleService.findAll();
		if(roles != null && roles.size() > 0) {
			return new ResponseEntity<List<Role>>(roles, HttpStatus.OK);
		}
		return new ResponseEntity<List<Role>>(HttpStatus.NOT_FOUND);
	}	
}
