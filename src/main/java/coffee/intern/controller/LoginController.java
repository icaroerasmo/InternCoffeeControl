package coffee.intern.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import coffee.intern.model.User;
import coffee.intern.service.UserService;

@Controller
public class LoginController {
	
	@Autowired
	UserService userService;

	@RequestMapping(value= {"/", "/login"}, method = RequestMethod.GET)
	public ModelAndView loginPage(){

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		if(!(auth instanceof AnonymousAuthenticationToken)) {
			return new ModelAndView("redirect:/success");
		}
		
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");
		return modelAndView;
	}
	
	@ResponseBody
	@RequestMapping(value= {"/success"}, method = RequestMethod.GET)
	public ResponseEntity<User> success(){
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		return new ResponseEntity<User>(userService.findByUsername(username), HttpStatus.OK);
	}
}