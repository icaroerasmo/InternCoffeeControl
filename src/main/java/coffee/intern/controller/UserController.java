package coffee.intern.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import coffee.intern.model.User;
import coffee.intern.service.UserService;
import coffee.intern.util.AuthUtil;

@RestController
@RequestMapping("/user")
public class UserController extends AuthUtil implements ICrudController<User> {

	@Autowired
	private UserService userService;

	public UserController() {

	}

	@Override
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<User>> get() {
		if (isAdmin()) {
			List<User> users = userService.findAll();
			if (users != null && users.size() > 0) {
				return new ResponseEntity<List<User>>(users, HttpStatus.OK);
			}
		}
		return new ResponseEntity<List<User>>(HttpStatus.NOT_FOUND);
	}

	@Override
	@RequestMapping(method = RequestMethod.GET, value = "{id}")
	public ResponseEntity<User> get(@PathVariable Integer id) {
		if (id != null) {
			User user = userService.findOne(id);
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (auth.getName().equals(user.getUsername()) || isAdmin()) {
				if (user != null) {
					return new ResponseEntity<User>(user, HttpStatus.OK);
				}
			}
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<User> save(@RequestBody User user) {
		if (isAdmin()) {
			if (user != null) {
				try {
					return new ResponseEntity<User>(userService.save(user), HttpStatus.OK);
				} catch (DataIntegrityViolationException e) {
					return new ResponseEntity<User>(HttpStatus.CONFLICT);
				}
			}
			return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
	}

	@Override
	@RequestMapping(method = RequestMethod.PATCH)
	public ResponseEntity<User> edit(@RequestBody User user) {
		if (isAdmin()) {
			if (user != null) {
				if (userService.findOne(user.getId()) != null) {
					return new ResponseEntity<User>(userService.merge(user), HttpStatus.OK);
				}
			}
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "{id}")
	public ResponseEntity<User> delete(@PathVariable Integer id) {
		if (isAdmin()) {
			if (id != null) {
				User user = userService.findOne(id);
				if (user != null) {
					if (!getLoggedUser().getName().equals(user.getUsername()) && !user.getId().equals(1)) {
						userService.delete(user);
					}
					return new ResponseEntity<User>(HttpStatus.OK);
				}
			}
			return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<User> deleteMany(@RequestBody List<User> users) {
		if (isAdmin()) {
			if (users.size() > 0) {

				List<User> notRemovableUsers = users.stream()
						.filter(item -> item.getId().equals(1) || item.getUsername().equals(getLoggedUser().getName()))
						.collect(Collectors.toList());

				if (notRemovableUsers != null && notRemovableUsers.size() > 0) {
					users.removeAll(notRemovableUsers);
				}

				userService.delete(users);
				return new ResponseEntity<User>(HttpStatus.OK);
			}
			return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(method = RequestMethod.GET, path = "byUsername")
	public ResponseEntity<User> getByName(@RequestParam(name = "username") String username) {
		if (username != null) {
			User user = userService.findByUsername(username);
			if (user != null) {
				return new ResponseEntity<User>(user, HttpStatus.OK);
			}
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
	}
}
