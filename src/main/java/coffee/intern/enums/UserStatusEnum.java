package coffee.intern.enums;

public enum UserStatusEnum {
	ACTIVE(1), INACTIVE(0);
	
	Integer status;
	
	UserStatusEnum(Integer status){
		this.status = status;
	}
	
	public Integer get() {
		return status;
	}
	
	public static UserStatusEnum getStatus(Integer status) {
		switch(status) {
		case 0:	
			return INACTIVE;
		case 1:
			return ACTIVE;
		default:
			return null;
		}
	}
}
