package coffee.intern.enums;

import coffee.intern.model.Intern;

public enum SelectionStatusEnum {
	COFFEE_MADE(-1),
	GAVE_UP(+1),
	COULDNT_MAKE(0);
	
	private int pontuation;
	
	private SelectionStatusEnum(int pontuation) {
		this.pontuation = pontuation;
	}
	
	public void setPontuation(Intern intern) {
		
		if(intern.getWeight() == null) {
			intern.setWeight(0);
		}
		
		if((this == COFFEE_MADE && intern.getWeight() > 0) || 
				this == GAVE_UP || this == COULDNT_MAKE) {
			intern.setWeight(intern.getWeight() + this.pontuation);
		}
	}
}
