package coffee.intern.service;

import java.util.List;

public interface IService<T> {
	public T save(T entity);
	public void delete(T entity);
	public void delete(Iterable<T> entities);
	public T findOne(Integer id);
	public List<T> findAll();
}
