package coffee.intern.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import coffee.intern.model.Intern;
import coffee.intern.repository.CoffeeRepository;
import coffee.intern.repository.InternRepository;
import coffee.intern.repository.SelectionRepository;

@Service("internService")
public class InternService implements IService<Intern> {

	@Autowired
	private InternRepository internRepository;
	
	@Autowired
	private CoffeeRepository coffeeRepository;
	
	@Autowired
	private SelectionRepository selectionRepository;

	@Override
	public Intern save(Intern entity) {
		return internRepository.save(entity);
	}

	@Override
	public void delete(Intern entity) {
		coffeeRepository.deleteCoffeeById(entity.getId());
		selectionRepository.deleteSelectionById(entity.getId());
		internRepository.delete(entity);
	}
	
	@Override
	public void delete(Iterable<Intern> entities) {
		entities.forEach(item -> {
			coffeeRepository.deleteCoffeeById(item.getId());
			selectionRepository.deleteSelectionById(item.getId());
			});
		internRepository.delete(entities);
	}

	@Override
	public Intern findOne(Integer id) {
		return internRepository.findOne(id);
	}

	@Override
	public List<Intern> findAll() {
		List<Intern> list = new ArrayList<Intern>();
		internRepository.findAll().forEach(list::add);
		return list;
	}
	
	public List<Intern> getCoffeeOrder(Pageable pageable){
		return internRepository.getInternOrder(pageable);
	}

	public Intern findByName(String name) {
		return internRepository.findByName(name);
	}
}