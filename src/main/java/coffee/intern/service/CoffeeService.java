package coffee.intern.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coffee.intern.model.Coffee;
import coffee.intern.repository.CoffeeRepository;

@Service("coffeeService")
public class CoffeeService {
	
	@Autowired
	private CoffeeRepository coffeeRepository;

	public Coffee save(Coffee entity) {
		return coffeeRepository.save(entity);
	}
	
	public List<Coffee> findLastWeekCoffees(){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, -7);
		return coffeeRepository.findLastWeekCoffees(calendar.getTime());
	}
	
	public Integer counterOfCoffeesByIntern(Integer internId) {
		return coffeeRepository.counterOfCoffeesByIntern(internId);
	}

	public List<Coffee> findAll() {
		List<Coffee> list = new ArrayList<Coffee>();
		coffeeRepository.findAll().forEach(list::add);
		return list;
	}
}
