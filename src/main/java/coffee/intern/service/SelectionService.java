package coffee.intern.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coffee.intern.model.Selection;
import coffee.intern.repository.SelectionRepository;

@Service("selectionService")
public class SelectionService implements IService<Selection> {

	@Autowired
	private SelectionRepository selectionRepository;
	
	@Override
	public Selection save(Selection entity) {
		return selectionRepository.save(entity);
	}

	@Override
	public void delete(Selection entity) {
		selectionRepository.delete(entity);
	}
	
	@Override
	public void delete(Iterable<Selection> entities) {
		selectionRepository.delete(entities);
	}

	@Override
	public Selection findOne(Integer id) {
		return selectionRepository.findOne(id);
	}

	@Override
	public List<Selection> findAll() {
		List<Selection> list = new ArrayList<Selection>();
		selectionRepository.findAll().forEach(list::add);
		return list;
	}
}
