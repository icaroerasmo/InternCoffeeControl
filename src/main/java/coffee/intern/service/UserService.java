package coffee.intern.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import coffee.intern.model.User;
import coffee.intern.repository.UserRepository;

@Service("userService")
public class UserService implements IService<User> {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public User save(User entity) {
		if(entity.getId() == null) {
			entity.setPassword(bCryptPasswordEncoder.encode(entity.getPassword()));
			return userRepository.save(entity);
		}
		throw new DataIntegrityViolationException("User already exists.");
	}

	@Override
	public void delete(User entity) {
		userRepository.delete(entity);
	}
	
	@Override
	public void delete(Iterable<User> entities) {
		userRepository.delete(entities);
	}

	@Override
	public User findOne(Integer id) {
		return userRepository.findOne(id);
	}

	@Override
	public List<User> findAll() {
		List<User> list = new ArrayList<User>();
		userRepository.findAll().forEach(list::add);
		return list;
	}
	
	public User merge(User entity) {
		if(entity.getId() != null) {
			User userToSave = userRepository.findOne(entity.getId());
			
			if(entity.getPassword() != null && !entity.getPassword().equals(userToSave.getPassword())) {
				userToSave.setPassword(bCryptPasswordEncoder.encode(entity.getPassword()));
			}
			
			if(entity.getName() != null) {
				userToSave.setName(entity.getName());
			}
			
			if(entity.getUsername() != null) {
				userToSave.setUsername(entity.getUsername());
			}
			
			if(entity.getRoles() != null) {
				userToSave.setRoles(entity.getRoles());
			}
			
			if(entity.getStatus() != null) {
				userToSave.setStatus(entity.getStatus());
			}
			
			return userRepository.save(userToSave);
		}
		throw new DataIntegrityViolationException("User doesn't exist.");
	}

	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}
}
