package coffee.intern.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coffee.intern.enums.RoleEnum;
import coffee.intern.model.Role;
import coffee.intern.repository.RoleRepository;

@Service("roleService")
public class RoleService implements IService<Role> {

	@Autowired
	private RoleRepository roleRepository;
	
	@Override
	public Role save(Role entity) {
		return roleRepository.save(entity);
	}

	@Override
	public void delete(Role entity) {
		roleRepository.delete(entity);		
	}
	
	@Override
	public void delete(Iterable<Role> entities) {
		roleRepository.delete(entities);
	}

	@Override
	public Role findOne(Integer id) {
		return roleRepository.findOne(id);
	}

	@Override
	public List<Role> findAll() {
		List<Role> roleList = new ArrayList<>();
		roleRepository.findAll().forEach(roleList::add);
		return roleList;
	}
	
	public Role findByRole(RoleEnum role){
		return roleRepository.findByRole(role);
	}
}
