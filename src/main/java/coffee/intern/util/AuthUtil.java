package coffee.intern.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import coffee.intern.enums.RoleEnum;

public abstract class AuthUtil {

	public boolean isAdmin() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return auth.getAuthorities().stream().anyMatch(item -> item.getAuthority().equals(RoleEnum.ADMIN.toString()));
	}
	
	public boolean isUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return auth.getAuthorities().stream().anyMatch(item -> item.getAuthority().equals(RoleEnum.USER.toString()));
	}
	
	public Authentication getLoggedUser() {
		return SecurityContextHolder.getContext().getAuthentication();
	}
}
